package edu.upc.damo.punts_mvc;


import android.view.MotionEvent;
import android.view.View;

import java.util.Random;

/**
 * Created by Josep M on 21/10/2015.
 */
public class GeneradorDePuntsEstatic {
    // Mètodes per a obtenir un punt
    // No té en compte què es fa amb aquest punt

    static final int DIAMETRE =25;
    static Random generador = new Random();

    /**
     * @vista defineix els límits del punt
     * @param event Conté la posició o s'ha de generar el punt
     * @param c Color del punt
     */
    static public Punt nouPunt(View vista, MotionEvent event, int c){
        return new Punt(event.getX(), event.getY(),c, DIAMETRE);
    }


    static public Punt nouPunt(View vista, int c) {
        /**
         * Genera un punt aleatori. El random retorna un valor entre 0 i 1, que considerem com una
         * fracció de l'espai disponible. Per això multipliquem el valor aleatori per la dimensió.
         * Per tal d'assegutar que el punt generat cap completament dins dels límits, hi sumem el diàmetre
         */

        int maxX;
        int maxY;

        maxX=vista.getWidth()-DIAMETRE;
        maxY=vista.getHeight()-DIAMETRE;

        generador = new Random();
        return new Punt(generador.nextFloat()*maxX+DIAMETRE/2,
                        generador.nextFloat()*maxY+DIAMETRE/2,
                        c, DIAMETRE);
    }



}


